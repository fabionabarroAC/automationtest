package com.ac.automationTest.steps;

import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import java.util.HashMap;
import static io.restassured.RestAssured.given;


public class BDDStyleMethod {

    public static void PerformPOSTWithBodyParameter(int port){
        HashMap<String,String> postContent = new HashMap<>();
        postContent.put("name", "fabio");
        postContent.put("email", "fabio@gmail.com");
        postContent.put("gender", "FEMALE");

        given().port(port)
                .contentType(ContentType.JSON).
                with()
                .body(postContent).
                when().filters(new ResponseLoggingFilter())
                .post("/api/v1/students").
                then().statusCode(200);
    }
}