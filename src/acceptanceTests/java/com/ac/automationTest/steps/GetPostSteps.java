package com.ac.automationTest.steps;

import io.cucumber.java.en.Given;
import io.restassured.response.Response;
import io.restassured.response.ResponseOptions;
import org.springframework.boot.web.server.LocalServerPort;


public class GetPostSteps {

    private static ResponseOptions<Response> response;
    @LocalServerPort
    private int port;
    @Given("I perform POST operation for {string}")
    public void iPerformPOSTOperationFor(String arg0) {
        BDDStyleMethod.PerformPOSTWithBodyParameter(port);
    }
}


