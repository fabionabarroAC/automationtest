package com.ac.automationTest;

public enum Gender {
    MALE,
    FEMALE,
    OTHER
}
