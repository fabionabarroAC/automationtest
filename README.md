# automationTest


## Unit test
./gradlew test

## Integration test
./gradlew integrationTest

## Acceptance test with Cucumber
./gradlew cucumber